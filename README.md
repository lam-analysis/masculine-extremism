
# Getting started

Most of the works in this repository, especially the `R` scripts, should
be directly reproducible. You’ll need
[`git`](https://git-scm.com/downloads),
[`R`](https://www.r-project.org/), and more conveniently [RStudio
IDE](https://posit.co/downloads/) installed and running well in your
system. You simply need to fork/clone this repository using RStudio by
following [this tutorial, start right away from
`Step 2`](https://book.cds101.com/using-rstudio-server-to-clone-a-github-repo-as-a-new-project.html#step---2).
In your RStudio command line, you can copy paste the following code to
setup your working directory:

    install.packages("renv") # Only need to run this step if `renv` is not installed

This step will install `renv` package, which will help you set up the
`R` environment. Please note that `renv` helps tracking, versioning, and
updating packages I used throughout the analysis.

    renv::restore()

This step will read `renv.lock` file and install required packages to
your local machine. When all packages loaded properly (make sure there’s
no error at all), you can proceed with:

    install.packages("targets")

This step will install `targets`, which I use to track my analysis
pipeline. The `targets` package will automate each analysis step,
starting from cleaning the data up to rendering a report. Run the
following command to complete the analysis:

    targets::tar_make()

This step will read `_targets.R` file, where I systematically draft all
of the analysis steps. Once it’s done running, you will find the
rendered document (either in `html` or `pdf`) inside the `render`
directory.

# What’s this all about?

``` mermaid
graph LR
  subgraph legend
    direction LR
    x7420bd9270f8d27d([""Up to date""]):::uptodate --- x5b3426b4c7fa7dbc([""Started""]):::started
    x5b3426b4c7fa7dbc([""Started""]):::started --- xbf4603d6c2c2ad6b([""Stem""]):::none
    xbf4603d6c2c2ad6b([""Stem""]):::none --- xf0bce276fe2b9d3e>""Function""]:::none
    xf0bce276fe2b9d3e>""Function""]:::none --- x5bffbffeae195fc9{{""Object""}}:::none
  end
  subgraph Graph
    direction LR
    x463b4d121a7009f3>"findCol"]:::uptodate --> x8ac6c11956bd1d3e>"mkForm"]:::uptodate
    x463b4d121a7009f3>"findCol"]:::uptodate --> x6caf5413452d015b>"vizPairs"]:::uptodate
    x463b4d121a7009f3>"findCol"]:::uptodate --> x4d22701f610751a5>"corSummary"]:::uptodate
    x463b4d121a7009f3>"findCol"]:::uptodate --> x48de5fd9e99c55ba>"tblSummary"]:::uptodate
    x58984eb91edaf63e>"scoreLavaan"]:::uptodate --> x01a9097d599bf6d3>"postScore"]:::uptodate
    x5ba76fe500875324(["tbl_score"]):::uptodate --> xed6fabb2050e179a(["plot_pair"]):::uptodate
    x6caf5413452d015b>"vizPairs"]:::uptodate --> xed6fabb2050e179a(["plot_pair"]):::uptodate
    xf76d768a8d313511>"fitLavaan"]:::uptodate --> x826cb5e824fb8db0(["mod_triad"]):::uptodate
    xdec8b6ea7b6b464f(["form_triad"]):::uptodate --> x826cb5e824fb8db0(["mod_triad"]):::uptodate
    xe797a538520c914d(["tbl"]):::uptodate --> x826cb5e824fb8db0(["mod_triad"]):::uptodate
    x826cb5e824fb8db0(["mod_triad"]):::uptodate --> x5ba76fe500875324(["tbl_score"]):::uptodate
    x01a9097d599bf6d3>"postScore"]:::uptodate --> x5ba76fe500875324(["tbl_score"]):::uptodate
    xe797a538520c914d(["tbl"]):::uptodate --> x5ba76fe500875324(["tbl_score"]):::uptodate
    xf7f7856cdd0737e0(["lv_dark"]):::uptodate --> x435661c82373df5d(["form_darks"]):::uptodate
    x8ac6c11956bd1d3e>"mkForm"]:::uptodate --> x435661c82373df5d(["form_darks"]):::uptodate
    xe797a538520c914d(["tbl"]):::uptodate --> x435661c82373df5d(["form_darks"]):::uptodate
    x9fd0c702895949b2(["mod_H1a"]):::uptodate --> x5b1b7835548cdc27(["summary_H1a"]):::uptodate
    xe16552a2ddb45779>"tblGLM"]:::uptodate --> x5b1b7835548cdc27(["summary_H1a"]):::uptodate
    x63bff8b3114b824a{{"H1a"}}:::uptodate --> x9fd0c702895949b2(["mod_H1a"]):::uptodate
    xe797a538520c914d(["tbl"]):::uptodate --> x9fd0c702895949b2(["mod_H1a"]):::uptodate
    xaf35cb5c79228483(["mod_H1b"]):::uptodate --> x0a94f70dd08ccc4e(["summary_H1b"]):::uptodate
    xe16552a2ddb45779>"tblGLM"]:::uptodate --> x0a94f70dd08ccc4e(["summary_H1b"]):::uptodate
    x8d10fe7ba0db4a94{{"H1b"}}:::uptodate --> xaf35cb5c79228483(["mod_H1b"]):::uptodate
    xe797a538520c914d(["tbl"]):::uptodate --> xaf35cb5c79228483(["mod_H1b"]):::uptodate
    xf76d768a8d313511>"fitLavaan"]:::uptodate --> xd971080e9bcea34f(["cfa_darks"]):::uptodate
    x435661c82373df5d(["form_darks"]):::uptodate --> xd971080e9bcea34f(["cfa_darks"]):::uptodate
    xe797a538520c914d(["tbl"]):::uptodate --> xd971080e9bcea34f(["cfa_darks"]):::uptodate
    xd971080e9bcea34f(["cfa_darks"]):::uptodate --> x8d98a08b4cf9377e(["report_eda"]):::uptodate
    x2f54b4c17fc90e3a(["cor_summary"]):::uptodate --> x8d98a08b4cf9377e(["report_eda"]):::uptodate
    x0196d35078fed42a(["group_summary"]):::uptodate --> x8d98a08b4cf9377e(["report_eda"]):::uptodate
    x4fe46358343c7f0d(["mod_H2"]):::uptodate --> x8d98a08b4cf9377e(["report_eda"]):::uptodate
    x826cb5e824fb8db0(["mod_triad"]):::uptodate --> x8d98a08b4cf9377e(["report_eda"]):::uptodate
    xed6fabb2050e179a(["plot_pair"]):::uptodate --> x8d98a08b4cf9377e(["report_eda"]):::uptodate
    x5b1b7835548cdc27(["summary_H1a"]):::uptodate --> x8d98a08b4cf9377e(["report_eda"]):::uptodate
    x0a94f70dd08ccc4e(["summary_H1b"]):::uptodate --> x8d98a08b4cf9377e(["report_eda"]):::uptodate
    xf76d768a8d313511>"fitLavaan"]:::uptodate --> x4fe46358343c7f0d(["mod_H2"]):::uptodate
    x969d804dec917bf3{{"H2"}}:::uptodate --> x4fe46358343c7f0d(["mod_H2"]):::uptodate
    xe797a538520c914d(["tbl"]):::uptodate --> x4fe46358343c7f0d(["mod_H2"]):::uptodate
    x4d22701f610751a5>"corSummary"]:::uptodate --> x2f54b4c17fc90e3a(["cor_summary"]):::uptodate
    x5ba76fe500875324(["tbl_score"]):::uptodate --> x2f54b4c17fc90e3a(["cor_summary"]):::uptodate
    x435661c82373df5d(["form_darks"]):::uptodate --> xdec8b6ea7b6b464f(["form_triad"]):::uptodate
    x5ba76fe500875324(["tbl_score"]):::uptodate --> x0196d35078fed42a(["group_summary"]):::uptodate
    x48de5fd9e99c55ba>"tblSummary"]:::uptodate --> x0196d35078fed42a(["group_summary"]):::uptodate
    x6d51284275156668(["file"]):::uptodate --> xe797a538520c914d(["tbl"]):::uptodate
    xb4a3d4a2fc7bb7d2>"readData"]:::uptodate --> xe797a538520c914d(["tbl"]):::uptodate
    x6e52cb0f1668cc22(["readme"]):::started --> x6e52cb0f1668cc22(["readme"]):::started
  end
  classDef uptodate stroke:#000000,color:#ffffff,fill:#354823;
  classDef started stroke:#000000,color:#000000,fill:#DC863B;
  classDef none stroke:#000000,color:#000000,fill:#94a4ac;
  linkStyle 0 stroke-width:0px;
  linkStyle 1 stroke-width:0px;
  linkStyle 2 stroke-width:0px;
  linkStyle 3 stroke-width:0px;
  linkStyle 49 stroke-width:0px;
```

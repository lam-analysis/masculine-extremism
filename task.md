Scoring & Computing (CFA)
Create single-total score militant extremist mindset (EFA/CFA result of vio1_r to exc8) -> DV is MEM
Create single-total score of NARC -> narc1 to narc4
Create single-total score of PSY -> psy1 to psy4
Create single-total score of MACH -> mach1 to mach4
Create 2 single-total score of attractiveness 
COMPUTE M1=SUM(pi3 to pi5).
COMPUTE M2=SUM(pi6 to pi8).
COMPUTE F1=SUM(pi9 to pi11).
COMPUTE F2=SUM(pi12 to pi14).
COMPUTE F3=SUM(pi15 to pi17).
COMPUTE F4=SUM(pi18 to pi20).
COMPUTE oppo=SUM(M1,F2).
COMPUTE self=SUM(M2,F4).

Report fit & weight.

Handling non-normal distribution -> just avoid non parametric test, use any means (transformation) u need, jika ga normal juga. Injek normality assumption, dan tunjukkin heterokedastesitas nanti. 

Descriptive

Gambaran perbedaan mean antara laki-laki dan perempuan di semua variabel. Mean, sd, n, t, and p.
MEM
NARC
PSY
MACH
oppo
self

Interkorelasi antar variabel
Sorted p1, MEM, narc, psy, mach, oppo, self, age, p2, edu, mos.
Feel free untuk adjust 2 or 1-tailed
Set index -> pearson (no kendall’s tau/spearman/biserial).
Hypotheses-testing

General rationale: berdasarkan environment contingent preference (Little, 2011), perempuan akan mengembangkan preferensi ke laki-laki yang memberikan direct benefits (pembelaan) dan indirect benefits (long-term protection).

H1 : Perempuan akan cenderung pro terhadap kekerasan yang dilakukan Will Smith. (cross tabulate p1 & p2 & odds ratio). Laki-laki condong tidak memiliki perbedaan proporsi.

H2 : Persepsi bahwa opposite sex menganggap kekerasan itu atraktif adalah mekanisme yang menjelaskan bagaimana maskulinitas memprediksi sikap pro kekerasan pada laki-laki. (p2 predict oppo predict vio; simple mediation). 

Direct effect sex to vio should be insignificant
Indirect effect sex to vio through oppo should be significant.

H3 : Pada sampel laki-laki, setelah mengontrol variabel demografi, termasuk kepribadian (dark triad) dan trait agresif, persepsi bahwa perempuan Indonesia menganggap kekerasan itu atraktif yang dialami laki-laki akan memprediksi militant-extremist-mindset. Rationale: Di perempuan, trait dark triad akan memprediksi MEM. MEM muncul akibat trait keinginan perempuan untuk mengendalikan laki-laki (machiavellianism), bukan kesukaan pada violence in itself. Sedangkan di laki-laki, trait dark tidak akan memprediksi MEM. Satu-satunya motif untuk mengakuisisi sikap pro kekerasan (MEM) adalah karena persepsi bahwa opposite sex menganggap kekerasan adalah hal yang atraktif.

Expected result Adj R-sq change, F (df, df error) = F value, MSE, p. 

Set split file, apply analisis di 2 set sample, laki-laki dan perempuan. 
Set 4 step analisis
Step 1:
3 prediktor; usia, pendidikan (ordinal), member to majority group religion (0 = non muslim, 1 = muslim)
Step 2:
 7 prediktor; semua yang di step 1 ditambahkan 3 komponen dark triad (NARC, PSY, MACH) dan 1 trait aggression (self)
Step 3:
8 prediktor; semua yang di step 1 dan 2, ditambahkan 1 variabel persepsi bahwa opposite sex menganggap kekerasan itu atraktif (oppo)
this is all

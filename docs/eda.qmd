---
format:
  pdf:
    fig-height: 10
    fig-width: 14
knitr:
  opts_chunk:
    echo: false
    warning: false
    message: false
    error: false
header-includes:
- \usepackage{lscape}
- \newcommand{\blandscape}{\begin{landscape}}
- \newcommand{\elandscape}{\end{landscape}}
---

```{r}

pkgs <- c("magrittr", "targets")
pkgs_load <- sapply(pkgs, library, character.only = TRUE)
options(digits = 2)

```

# Descriptive Statistics

```{r}

tar_read(group_summary)
tar_read(cor_summary) %>% knitr::kable(format = "latex", booktabs = TRUE)

```

{{< pagebreak >}}

\blandscape

```{r}

tar_read(plot_pair)

```

\elandscape

{{< pagebreak >}}

# Model Fitting

```{r, results = "asis"}

tar_read(cfa_darks) %>% lapply(\(cfa) broom::tidy(cfa) %>% knitr::kable())

```

```{r}

tar_read(mod_triad) %>% broom::tidy() %>% knitr::kable()
tar_read(mod_triad) %>% semPlot::semPaths(nCharNodes = 0, whatLabels = "std")

```

# Hypothesis Testing

```{r}

tar_read(summary_H1a)
tar_read(summary_H1b)
tar_read(mod_H2) %>% broom::tidy() %>% knitr::kable()
tar_read(mod_H2) %>% semPlot::semPaths(nCharNodes = 0, whatLabels = "std")

```

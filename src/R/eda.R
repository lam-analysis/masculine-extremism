# Functions for conducting an exploratory data analysis

tblSummary <- function(tbl, regex) {
  #' Table Summary
  #'
  #' Create a descriptive statsitics table summary for variables matching the
  #' regular expression pattern
  #'
  #' @param tbl A data frame containing variable of interest
  #' @param regex A regular expression pattern
  #' @return A `kable` object containing the statistical summary
  varname <- findCol(tbl, regex)
  sub_tbl <- subset(tbl, select = c("pi2", varname))

  res <- sub_tbl %>%
    gtsummary::tbl_summary(
      by = pi2,
      statistic = list(gtsummary::all_continuous() ~ "{mean} [{sd}]")
    ) %>%
    gtsummary::add_difference() %>%
    gtsummary::modify_header(
      update = list(gtsummary::all_stat_cols() ~ "**{level}**\nN = {n}")
    ) %>%
    gtsummary::as_gt()

  return(res)
}

corSummary <- function(tbl, regex = NULL) {
  #' Correlation Summary
  #'
  #' Create a table of pairwise correlation summary for variables matching the
  #' regular expression pattern
  #'
  #' @param tbl A data frame containing variable of interest
  #' @param regex A regular expression pattern
  #' @return A `huxtable` object containing the statistical summary
  if (!is.null(regex)) {
    sub_tbl <- findCol(tbl, regex, fetch = TRUE)
  } else {
    sub_tbl <- tbl
  }

  sub_tbl %<>% lapply(as.numeric) %>% as.data.frame()

  res <- cor(sub_tbl, use = "complete.obs") %>%
    modelsummary::datasummary_correlation_format(
      fmt = 2, upper_triangle = "."
    ) %>%
    set_colnames(1:ncol(.)) %>%
    set_rownames(sprintf("[%s] %s", 1:nrow(.), names(sub_tbl)))

  return(res)
}

tblGLM <- function(model, ...) {
  #' Table of Model
  #'
  #' Summarize findings from a GLM model
  #'
  #' @param model A GLM model
  #' @param tbl A data frame containing all required variables
  #' @param ... Arguments being passed on to `base::glm`
  #' @return A `gtsummary` object
  #mod <- model %>% step()
  res <- gtsummary::tbl_regression(model, ...) %>%
    gtsummary::add_glance_table() %>%
    gtsummary::as_kable_extra()

  return(res)
}
